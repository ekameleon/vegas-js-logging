"use strict" ;

import Enum from 'vegas-js-system-core/src/Enum.js'

/**
 * The logger levels that is used within the logging framework.
 * @summary The logger levels that is used within the logging framework.
 * @name LoggerLevel
 * @class
 * @memberof system.logging
 * @extends system.Enum
 * @example
 * for( var level in LoggerLevel )
 * {
 *     if( LoggerLevel.hasOwnProperty(level) )
 *     {
 *        trace( level + ' ' + LoggerLevel.getLevelString(LoggerLevel[level]) ) ;
 *     }
 * }
 */
export default class LoggerLevel extends Enum
{
    /**
     * @constructor
     * @param {number} value - The value of the enumeration.
     * @param {string} name - The name key of the enumeration.
     */
    constructor( value , name )
    {
        super( value , name ) ;
    }
    
    /**
     * Intended to force a target to process all messages (1).
     * @name ALL
     * @memberof system.logging.LoggerLevel
     * @static
     */
    static ALL = new LoggerLevel( 1 , 'ALL' ) ;

    /**
     * Designates events that are very harmful and will eventually lead to application failure (16).
     * @name CRITICAL
     * @memberof system.logging.LoggerLevel
     * @static
     */
    static CRITICAL = new LoggerLevel( 16 , 'CRITICAL' ) ;

    /**
     * Designates informational level messages that are fine grained and most helpful when debugging an application (2).
     * @name DEBUG
     * @memberof system.logging.LoggerLevel
     * @static
     */
    static DEBUG = new LoggerLevel( 2 , 'DEBUG' ) ;

    /**
     * The default string level value in the <code>getLevelString()</code> method ('UNKNOW').
     * @name DEFAULT_LEVEL_STRING
     * @memberof system.logging.LoggerLevel
     * @type string
     */
    static DEFAULT_LEVEL_STRING = 'UNKNOWN' ;

    /**
     * Designates error events that might still allow the application to continue running (8).
     * @name ERROR
     * @memberof system.logging.LoggerLevel
     * @static
     */
    static ERROR = new LoggerLevel( 8 , 'ERROR' ) ;

    /**
     * Designates informational messages that highlight the progress of the application at coarse-grained level (4).
     * @name INFO
     * @memberof system.logging.LoggerLevel
     * @static
     */
    static INFO = new LoggerLevel( 4 , 'INFO' ) ;

    /**
     * A special level that can be used to turn off logging (0).
     * @name NONE
     * @memberof system.logging.LoggerLevel
     * @static
     */
    static NONE = new LoggerLevel( 0 , 'NONE' ) ;

    /**
     * Designates events that could be harmful to the application operation (6).
     * @name WARNING
     * @memberof system.logging.LoggerLevel
     * @static
     */
    static WARNING = new LoggerLevel( 6 , 'WARNING' ) ;

    /**
     * What a Terrible Failure: designates an exception that should never happen. (32).
     * @name WTF
     * @memberof system.logging.LoggerLevel
     * @static
     */
    static WTF = new LoggerLevel( 32 , 'WTF' ) ;

    /**
     * Search a <code>LoggerLevel</code> reference if the number level passed in argument is valid.
     * @name get
     * @memberof system.logging.LoggerLevel
     * @function
     * @param {number} value - The numeric value corresponding to a valid LoggerLevel object.
     * @return {system.logging.LoggerLevel} The LoggerLevel reference.
     */
    static get( value )
    {
        let levels =
        [
            LoggerLevel.ALL,
            LoggerLevel.CRITICAL,
            LoggerLevel.DEBUG,
            LoggerLevel.ERROR,
            LoggerLevel.INFO,
            LoggerLevel.NONE,
            LoggerLevel.WARNING,
            LoggerLevel.WTF
        ] ;
        let l = levels.length ;
        while( --l > -1 )
        {
            if ( levels[l]._value === value )
            {
                return levels[l] ;
            }
        }
        return null ;
    }

    /**
     * Returns a String value representing the specific level.
     * @return {string} ga String value representing the specific level.
     * @name getLevelString
     * @memberof system.logging.LoggerLevel
     * @function
     */
    static getLevelString( value )
    {
        if ( LoggerLevel.validate( value ) )
        {
            return value.toString() ;
        }
        else
        {
            return LoggerLevel.DEFAULT_LEVEL_STRING ;
        }
    }

    /**
     * Validates a passed-in level passed in argument.
     * @return <code>true</code> if the level passed in argument is valid.
     * @name validate
     * @memberof system.logging.LoggerLevel
     * @function
     */
    static validate( level )
    {
        let levels =
        [
            LoggerLevel.ALL,
            LoggerLevel.CRITICAL,
            LoggerLevel.DEBUG,
            LoggerLevel.ERROR,
            LoggerLevel.INFO,
            LoggerLevel.NONE,
            LoggerLevel.WARNING,
            LoggerLevel.WTF
        ] ;
        return levels.indexOf( level ) > -1 ;
    }
}