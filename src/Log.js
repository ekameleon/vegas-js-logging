"use strict" ;

import LoggerFactory from './LoggerFactory'

/**
 * The singleton factory to generates all the <b>loggers</b> in your application.
 * @summary The singleton factory to generates all the <b>loggers</b> in your application.
 * @name Log
 * @memberof system.logging
 * @const
 * @instance
 */
const Log = new LoggerFactory() ;

export default Log  ;