"use strict" ;

import Loggable from './Loggable'
import Logger   from './Logger'

/**
 * Indicates if the specific object is {@link system.logging.Loggable|Loggable}.
 * @name isLoggable
 * @memberof system.logging
 * @function
 * @instance
 * @param {object} target - The object to evaluate.
 * @return {boolean} <code>true</code> if the object is <code>Loggable</code>.
 */
export default function isLoggable( target )
{
    if( target )
    {
        return target instanceof Loggable ||
               (( 'logger' in target ) && ((target.logger === null ) || ( target.logger instanceof Logger )) )  ;
    }
    return false ;
}