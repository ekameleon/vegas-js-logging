"use strict" ;

import Logger from './Logger'

/**
 * Implementing this interface allows an object who use a {@link system.logging.Logger|Logger} object.
 * @name Loggable
 * @memberof system.logging
 * @interface
 */
export default function Loggable()
{
    Object.defineProperties( this ,
    {
        _logger : { value : null , writable : true }
    }) ;
}

Loggable.prototype = Object.create( Object.prototype ,
{
    constructor : { value : Loggable } ,

    /**
     * Determinates the internal <code>Logger</code> reference of this <code>Loggable</code> object.
     * @name logger
     * @memberof system.logging.Loggable
     * @type {system.logging.Logger}
     * @instance
     */
    logger :
    {
        get : function() { return this._logger ; },
        set : function( logger )
        {
            this._logger = (logger instanceof Logger) ? logger : null ;
        }
    }
});