"use strict" ;

import LoggerLevel from './LoggerLevel'

/**
 * Represents the log information for a single logging notification.
 * <p>The logging system dispatches a single message each time a process requests information be logged.</p>
 * <p>This entry can be captured by any object for storage or formatting.</p>
 * @name LoggerEntry
 * @memberof system.logging
 * @class
 */
export default class LoggerEntry
{
    /**
     * @constructor
     * @param {string} message - The context or message of the log.
     * @param {system.logging.LoggerLevel} level - The level of the log.
     * @param {string} channel - The channel of the log.
     */
    constructor( message , level , channel )
    {
        /**
         * The channel of the log.
         * @memberof system.logging.LoggerEntry
         * @instance
         * @type string
         */
        this.channel = channel ;
    
        /**
         * The level of the log.
         * @memberof system.logging.LoggerEntry
         * @instance
         * @type system.logging.LoggerLevel
         */
        this.level = level instanceof LoggerLevel ? level : LoggerLevel.ALL ;
    
        /**
         * The message of the log.
         * @memberof system.logging.LoggerEntry
         * @instance
         * @type string
         */
        this.message = message ;
    }
}