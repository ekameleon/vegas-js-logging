/**
 * A library allow you to implement a flexible event logging system for applications and libraries. - version: 1.0.2 - license: MPL 2.0/GPL 2.0+/LGPL 2.1+ - Follow me on Twitter! @ekameleon
 */

(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global.vegas_logging = factory());
}(this, (function () { 'use strict';

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};
      var ownKeys = Object.keys(source);

      if (typeof Object.getOwnPropertySymbols === 'function') {
        ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) {
          return Object.getOwnPropertyDescriptor(source, sym).enumerable;
        }));
      }

      ownKeys.forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    }

    return target;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    if (superClass) _setPrototypeOf(subClass, superClass);
  }

  function _getPrototypeOf(o) {
    _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf(o);
  }

  function _setPrototypeOf(o, p) {
    _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };

    return _setPrototypeOf(o, p);
  }

  function _assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return self;
  }

  function _possibleConstructorReturn(self, call) {
    if (call && (typeof call === "object" || typeof call === "function")) {
      return call;
    }

    return _assertThisInitialized(self);
  }

  class Receiver
  {
      constructor()
      {
      }
      receive() {}
      toString()
      {
          return '[' + this.constructor.name + ']' ;
      }
  }

  class Signaler
  {
      constructor()
      {
      }
      get length()
      {
          return 0 ;
      }
      connect( receiver , priority = 0 , autoDisconnect = false )
      {
      }
      connected()
      {
      }
      disconnect( receiver = null ) {} ;
      emit( ...values )
      {
      }
      hasReceiver( receiver )
      {
          return false ;
      }
      toString()
      {
          return '[Signaler]' ;
      }
  }

  class Signal extends Signaler
  {
      constructor()
      {
          super() ;
          Object.defineProperties( this ,
          {
              proxy : { value : null, configurable : true , writable : true } ,
              receivers : { writable : true , value : [] }
          }) ;
      }
      get length()
      {
          return this.receivers.length ;
      }
      connect( receiver , priority = 0 , autoDisconnect = false )
      {
          if ( receiver === null )
          {
              return false ;
          }
          autoDisconnect = autoDisconnect === true ;
          priority = priority > 0 ? (priority - (priority % 1)) : 0 ;
          if ( ( typeof(receiver) === "function" ) || ( receiver instanceof Function ) || ( receiver instanceof Receiver ) || ( "receive" in receiver ) )
          {
              if ( this.hasReceiver( receiver ) )
              {
                  return false ;
              }
              this.receivers.push( new SignalEntry( receiver , priority , autoDisconnect ) ) ;
              let i ;
              let j ;
              let a = this.receivers ;
              let swap = function( j , k )
              {
                  let temp = a[j] ;
                  a[j]     = a[k] ;
                  a[k]     = temp ;
                  return true ;
              };
              let swapped = false;
              let l = a.length ;
              for( i = 1 ; i < l ; i++ )
              {
                  for( j = 0 ; j < ( l - i ) ; j++ )
                  {
                      if ( a[j+1].priority > a[j].priority )
                      {
                          swapped = swap(j, j+1) ;
                      }
                  }
                  if ( !swapped )
                  {
                      break;
                  }
              }
              return true ;
          }
          return false ;
      }
      connected()
      {
          return this.receivers.length > 0 ;
      }
      disconnect( receiver = null )
      {
          if ( receiver === null )
          {
              if ( this.receivers.length > 0 )
              {
                  this.receivers = [] ;
                  return true ;
              }
              else
              {
                  return false ;
              }
          }
          if ( this.receivers.length > 0 )
          {
              let l = this.receivers.length ;
              while( --l > -1 )
              {
                  if ( this.receivers[l].receiver === receiver )
                  {
                      this.receivers.splice( l , 1 ) ;
                      return true ;
                  }
              }
          }
          return false ;
      }
      emit( ...values )
      {
          let l = this.receivers.length ;
          if ( l === 0 )
          {
              return ;
          }
          let i ;
          let r = [] ;
          let a = this.receivers.slice() ;
          let e ;
          let slot ;
          for ( i = 0 ; i < l ; i++ )
          {
              e = a[i] ;
              if ( e.auto )
              {
                  r.push( e )  ;
              }
          }
          if ( r.length > 0 )
          {
              l = r.length ;
              while( --l > -1 )
              {
                  i = this.receivers.indexOf( r[l] ) ;
                  if ( i > -1 )
                  {
                      this.receivers.splice( i , 1 ) ;
                  }
              }
          }
          l = a.length ;
          for ( i = 0 ; i<l ; i++ )
          {
              slot = a[i].receiver ;
              if( slot instanceof Function || typeof(slot) === "function" )
              {
                  slot.apply( this.proxy || this , values ) ;
              }
              else if ( slot instanceof Receiver || ( "receive" in slot && (slot.receive instanceof Function) ) )
              {
                  slot.receive.apply( this.proxy || slot , values ) ;
              }
          }
      }
      hasReceiver( receiver )
      {
          if ( receiver === null )
          {
              return false ;
          }
          if ( this.receivers.length > 0 )
          {
              let l = this.receivers.length ;
              while( --l > -1 )
              {
                  if ( this.receivers[l].receiver === receiver )
                  {
                      return true ;
                  }
              }
          }
          return false ;
      }
      toArray()
      {
          if ( this.receivers.length > 0 )
          {
              return this.receivers.map( item => item.receiver ) ;
          }
          return [] ;
      }
      toString()
      {
          return '[Signal]' ;
      }
  }
  class SignalEntry
  {
      constructor( receiver , priority = 0 , auto = false )
      {
          this.auto = auto ;
          this.receiver = receiver ;
          this.priority = priority ;
      }
      toString () { return '[SignalEntry]' ; }
  }

  function Enum( value , name )
  {
      Object.defineProperties( this ,
      {
          _name :
          {
              value        : ((typeof(name) === "string" || name instanceof String )) ? name : "" ,
              enumerable   : false ,
              writable     : true ,
              configurable : true
          },
          _value :
          {
              value        : isNaN(value) ? 0 : value ,
              enumerable   : false ,
              writable     : true ,
              configurable : true
          }
      }) ;
  }
  Enum.prototype = Object.create( Object.prototype );
  Enum.prototype.constructor = Enum;
  Enum.prototype.equals = function ( object )
  {
      if ( object === this )
      {
          return true ;
      }
      if( object instanceof Enum )
      {
          return (object.toString() === this.toString()) && (object.valueOf() === this.valueOf()) ;
      }
      return false ;
  };
  Enum.prototype.toString = function()
  {
      return this._name ;
  };
  Enum.prototype.valueOf = function()
  {
      return this._value ;
  };

  var LoggerLevel =
  function (_Enum) {
    _inherits(LoggerLevel, _Enum);
    function LoggerLevel(value, name) {
      _classCallCheck(this, LoggerLevel);
      return _possibleConstructorReturn(this, _getPrototypeOf(LoggerLevel).call(this, value, name));
    }
    _createClass(LoggerLevel, null, [{
      key: "get",
      value: function get(value) {
        var levels = [LoggerLevel.ALL, LoggerLevel.CRITICAL, LoggerLevel.DEBUG, LoggerLevel.ERROR, LoggerLevel.INFO, LoggerLevel.NONE, LoggerLevel.WARNING, LoggerLevel.WTF];
        var l = levels.length;
        while (--l > -1) {
          if (levels[l]._value === value) {
            return levels[l];
          }
        }
        return null;
      }
    }, {
      key: "getLevelString",
      value: function getLevelString(value) {
        if (LoggerLevel.validate(value)) {
          return value.toString();
        } else {
          return LoggerLevel.DEFAULT_LEVEL_STRING;
        }
      }
    }, {
      key: "validate",
      value: function validate(level) {
        var levels = [LoggerLevel.ALL, LoggerLevel.CRITICAL, LoggerLevel.DEBUG, LoggerLevel.ERROR, LoggerLevel.INFO, LoggerLevel.NONE, LoggerLevel.WARNING, LoggerLevel.WTF];
        return levels.indexOf(level) > -1;
      }
    }]);
    return LoggerLevel;
  }(Enum);
  _defineProperty(LoggerLevel, "ALL", new LoggerLevel(1, 'ALL'));
  _defineProperty(LoggerLevel, "CRITICAL", new LoggerLevel(16, 'CRITICAL'));
  _defineProperty(LoggerLevel, "DEBUG", new LoggerLevel(2, 'DEBUG'));
  _defineProperty(LoggerLevel, "DEFAULT_LEVEL_STRING", 'UNKNOWN');
  _defineProperty(LoggerLevel, "ERROR", new LoggerLevel(8, 'ERROR'));
  _defineProperty(LoggerLevel, "INFO", new LoggerLevel(4, 'INFO'));
  _defineProperty(LoggerLevel, "NONE", new LoggerLevel(0, 'NONE'));
  _defineProperty(LoggerLevel, "WARNING", new LoggerLevel(6, 'WARNING'));
  _defineProperty(LoggerLevel, "WTF", new LoggerLevel(32, 'WTF'));

  var LoggerEntry =
  function LoggerEntry(message, level, channel) {
    _classCallCheck(this, LoggerEntry);
    this.channel = channel;
    this.level = level instanceof LoggerLevel ? level : LoggerLevel.ALL;
    this.message = message;
  };

  var Logger =
  function (_Signal) {
    _inherits(Logger, _Signal);
    function Logger(channel) {
      var _this;
      _classCallCheck(this, Logger);
      _this = _possibleConstructorReturn(this, _getPrototypeOf(Logger).call(this));
      Object.defineProperties(_assertThisInitialized(_assertThisInitialized(_this)), {
        _entry: {
          value: new LoggerEntry(null, null, channel),
          writable: true
        }
      });
      return _this;
    }
    return Logger;
  }(Signal);
  Logger.prototype = Object.create(Signal.prototype, {
    channel: {
      get: function get() {
        return this._entry.channel;
      }
    },
    critical: {
      value: function value(context) {
        for (var _len = arguments.length, options = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
          options[_key - 1] = arguments[_key];
        }
        this._log(LoggerLevel.CRITICAL, context, options);
      }
    },
    debug: {
      value: function value(context) {
        for (var _len2 = arguments.length, options = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
          options[_key2 - 1] = arguments[_key2];
        }
        this._log(LoggerLevel.DEBUG, context, options);
      }
    },
    error: {
      value: function value(context) {
        for (var _len3 = arguments.length, options = new Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {
          options[_key3 - 1] = arguments[_key3];
        }
        this._log(LoggerLevel.ERROR, context, options);
      }
    },
    info: {
      value: function value(context) {
        for (var _len4 = arguments.length, options = new Array(_len4 > 1 ? _len4 - 1 : 0), _key4 = 1; _key4 < _len4; _key4++) {
          options[_key4 - 1] = arguments[_key4];
        }
        this._log(LoggerLevel.INFO, context, options);
      }
    },
    log: {
      value: function value(context) {
        for (var _len5 = arguments.length, options = new Array(_len5 > 1 ? _len5 - 1 : 0), _key5 = 1; _key5 < _len5; _key5++) {
          options[_key5 - 1] = arguments[_key5];
        }
        this._log(LoggerLevel.ALL, context, options);
      }
    },
    warning: {
      value: function value(context) {
        for (var _len6 = arguments.length, options = new Array(_len6 > 1 ? _len6 - 1 : 0), _key6 = 1; _key6 < _len6; _key6++) {
          options[_key6 - 1] = arguments[_key6];
        }
        this._log(LoggerLevel.WARNING, context, options);
      }
    },
    wtf: {
      value: function value(context) {
        for (var _len7 = arguments.length, options = new Array(_len7 > 1 ? _len7 - 1 : 0), _key7 = 1; _key7 < _len7; _key7++) {
          options[_key7 - 1] = arguments[_key7];
        }
        this._log(LoggerLevel.WTF, context, options);
      }
    },
    toString: {
      value: function value() {
        return '[Logger]';
      }
    },
    _log: {
      value: function value(level, context, options) {
        if (this.connected()) {
          if ((typeof context === "string" || context instanceof String) && options instanceof Array) {
            var len = options.length;
            for (var i = 0; i < len; i++) {
              context = String(context).replace(new RegExp("\\{" + i + "\\}", "g"), options[i]);
            }
          }
          this._entry.message = context;
          this._entry.level = level;
          this.emit(this._entry);
        }
      }
    }
  });

  function Loggable() {
    Object.defineProperties(this, {
      _logger: {
        value: null,
        writable: true
      }
    });
  }
  Loggable.prototype = Object.create(Object.prototype, {
    constructor: {
      value: Loggable
    },
    logger: {
      get: function get() {
        return this._logger;
      },
      set: function set(logger) {
        this._logger = logger instanceof Logger ? logger : null;
      }
    }
  });

  function isLoggable(target) {
    if (target) {
      return target instanceof Loggable || 'logger' in target && (target.logger === null || target.logger instanceof Logger);
    }
    return false;
  }

  function indexOfAny( source , anyOf , startIndex = 0 , count = -1 )
  {
      if( !(source instanceof String || typeof(source) === 'string' ) || source === "" )
      {
          return -1 ;
      }
      if( !(anyOf instanceof Array) )
      {
          return -1 ;
      }
      startIndex = startIndex > 0 ? 0 : startIndex ;
      count      = count < 0 ? -1 : count ;
      let l = source.length ;
      let endIndex ;
      if( ( count < 0 ) || ( count > l - startIndex ) )
      {
          endIndex = l - 1 ;
      }
      else
      {
          endIndex = startIndex + count - 1;
      }
      for( let i = startIndex ; i <= endIndex ; i++ )
      {
          if( anyOf.indexOf( source[i] ) > - 1 )
          {
              return i ;
          }
      }
      return -1 ;
  }

  function KeyValuePair() {}
  KeyValuePair.prototype = Object.create( Object.prototype ,
  {
      constructor : { writable : true , value :  KeyValuePair },
      length : { get : () => 0 } ,
      clear : { value : function () {} , writable : true } ,
      clone : { value : function () { return new KeyValuePair() } , writable : true } ,
      copyFrom : { value : function( map ) {} , writable : true } ,
      delete : { value : function( key ) {} , writable : true } ,
      forEach : { value : function( callback , thisArg = null ) {} , writable : true } ,
      get : { value : function( key ) { return null ; } , writable : true } ,
      has : { value : function( key ) { return false ; } , writable : true } ,
      hasValue : { value : function( value ) { return false ; } , writable : true } ,
      isEmpty : { value : function() { return false ; } , writable : true } ,
      iterator : { value : function()              { return null ; } , writable : true } ,
      keyIterator : { value : function()              { return null } , writable : true } ,
      keys : { value : function() { return null ; } , writable : true } ,
      set : { value : function( key , value ) {} , writable : true } ,
      toString : { value : function () { return '[' + this.constructor.name + ']' ; } , writable : true } ,
      values : { value : function ()  {} , writable : true }
  }) ;

  function MapFormatter() {}
  MapFormatter.prototype = Object.create( Object.prototype ,
  {
      constructor : { writable : true , value : MapFormatter },
      format : { value : function( value )
      {
          if ( value instanceof KeyValuePair )
          {
              let r = "{";
              let keys = value.keys()   ;
              let len  = keys.length ;
              if( len > 0 )
              {
                  let values = value.values() ;
                  for( let i = 0 ; i < len ; i++ )
                  {
                      r += keys[i] + ':' + values[i] ;
                      if( i < len - 1 )
                      {
                          r += "," ;
                      }
                  }
              }
              r += "}" ;
              return r ;
          }
          else
          {
              return "{}" ;
          }
      }}
  }) ;

  const formatter = new MapFormatter() ;

  function MapEntry( key , value )
  {
      Object.defineProperties( this ,
      {
          key : { value : key , writable : true },
          value : { value : value , writable : true }
      }) ;
  }
  MapEntry.prototype = Object.create( Object.prototype ,
  {
      constructor : { value : MapEntry } ,
      clone : { value : function()
      {
          return new MapEntry( this.key , this.value ) ;
      }},
      toString : { value : function()
      {
          return "[MapEntry key:" + this.key + " value:" + this.value + "]" ;
      }}
  }) ;

  function Iterator() {}
  Iterator.prototype = Object.create( Object.prototype ,
  {
      constructor : { writable : true , value : Iterator } ,
      delete : { writable : true , value : function() {} } ,
      hasNext : { writable : true , value : function() {} } ,
      key : { writable : true , value : function() {} } ,
      next : { writable : true , value : function() {} } ,
      reset : { writable : true , value : function() {} } ,
      seek : { writable : true , value : function ( position ) {} } ,
      toString :
      {
          writable : true , value : function ()
          {
              return '[' + this.constructor.name + ']' ;
          }
      }
  }) ;

  function ArrayIterator( array )
  {
      if ( !(array instanceof Array) )
      {
          throw new ReferenceError( this + " constructor failed, the passed-in Array argument not must be 'null'.") ;
      }
      Object.defineProperties( this ,
      {
          _a : { value : array , writable : true } ,
          _k : { value : -1    , writable : true }
      }) ;
  }
  ArrayIterator.prototype = Object.create( Iterator.prototype ,
  {
      constructor : { value : ArrayIterator } ,
      delete : { value : function()
      {
          return this._a.splice(this._k--, 1)[0] ;
      }},
      hasNext : { value : function()
      {
          return (this._k < this._a.length -1) ;
      }},
      key : { value : function()
      {
          return this._k ;
      }},
      next : { value : function()
      {
          return this._a[++this._k] ;
      }},
      reset : { value : function()
      {
          this._k = -1 ;
      }},
      seek : { value : function ( position )
      {
          position = Math.max( Math.min( position - 1 , this._a.length ) , -1 ) ;
          this._k = isNaN(position) ? -1 : position ;
      }}
  }) ;

  function MapIterator( map )
  {
      if ( map && ( map instanceof KeyValuePair) )
      {
          Object.defineProperties( this ,
          {
              _m : { value : map  , writable : true } ,
              _i : { value : new ArrayIterator( map.keys() ) , writable : true } ,
              _k : { value : null , writable : true }
          }) ;
      }
      else
      {
         throw new ReferenceError( this + " constructor failed, the passed-in KeyValuePair argument not must be 'null'.") ;
      }
  }
  MapIterator.prototype = Object.create( Iterator.prototype ,
  {
      constructor : { writable : true , value : MapIterator } ,
      delete : { value : function()
      {
          this._i.delete() ;
          return this._m.delete( this._k ) ;
      }},
      hasNext : { value : function()
      {
          return this._i.hasNext() ;
      }},
      key : { value : function()
      {
          return this._k ;
      }},
      next : { value : function()
      {
          this._k = this._i.next() ;
          return this._m.get( this._k ) ;
      }},
      reset : { value : function()
      {
          this._i.reset() ;
      }},
      seek : { value : function ( position )
      {
          throw new Error( "This Iterator does not support the seek() method.") ;
      }}
  }) ;

  function ArrayMap( keys = null , values = null )
  {
      Object.defineProperties( this ,
      {
          _keys :
          {
              value : [] ,
              writable : true
          },
          _values :
          {
              value : [] ,
              writable : true
          }
      }) ;
      if ( keys === null || values === null )
      {
          this._keys   = [] ;
          this._values = [] ;
      }
      else
      {
          let b = ( keys instanceof Array && values instanceof Array && keys.length > 0 && keys.length === values.length ) ;
          this._keys   = b ? [].concat(keys)   : [] ;
          this._values = b ? [].concat(values) : [] ;
      }
  }
  ArrayMap.prototype = Object.create( KeyValuePair.prototype ,
  {
      constructor : { writable : true , value : ArrayMap } ,
      length : { get : function() { return this._keys.length ; } } ,
      clear : { value : function ()
      {
          this._keys   = [] ;
          this._values = [] ;
      }},
      clone : { value : function ()
      {
          return new ArrayMap( this._keys , this._values ) ;
      }},
      copyFrom : { value : function ( map )
      {
          if ( !map || !(map instanceof KeyValuePair) )
          {
              return ;
          }
          let keys = map.keys() ;
          let values = map.values() ;
          let l = keys.length ;
          for ( let i = 0 ; i<l ; i = i - (-1) )
          {
              this.set(keys[i], values[i]) ;
          }
      }},
      delete : { value : function ( key )
      {
          let v = null ;
          let i = this.indexOfKey( key ) ;
          if ( i > -1 )
          {
              v = this._values[i] ;
              this._keys.splice(i, 1) ;
              this._values.splice(i, 1) ;
          }
          return v ;
      }},
      forEach : { value : function( callback , thisArg = null )
      {
          if (typeof callback !== "function")
          {
              throw new TypeError( callback + ' is not a function' );
          }
          let l = this._keys.length ;
          for( let i = 0 ; i<l ; i++ )
          {
              callback.call( thisArg , this._values[i] , this._keys[i] , this ) ;
          }
      }},
      get : { value : function( key )
      {
          return this._values[ this.indexOfKey( key ) ] ;
      }},
      getKeyAt : { value : function ( index )
      {
          return this._keys[ index ] ;
      }},
      getValueAt : { value : function ( index          )
      {
          return this._values[ index ] ;
      }},
      has : { value : function ( key )
      {
          return this.indexOfKey(key) > -1 ;
      }},
      hasValue : { value : function ( value )
      {
          return this.indexOfValue( value ) > -1 ;
      }},
      indexOfKey : { value : function (key)
      {
          let l = this._keys.length ;
          while( --l > -1 )
          {
              if ( this._keys[l] === key )
              {
                  return l ;
              }
          }
          return -1 ;
      }},
      indexOfValue : { value : function (value)
      {
          let l = this._values.length ;
          while( --l > -1 )
          {
              if ( this._values[l] === value )
              {
                  return l ;
              }
          }
          return -1 ;
      }},
      isEmpty : { value : function ()
      {
          return this._keys.length === 0 ;
      }},
      iterator : { value : function ()
      {
          return new MapIterator( this ) ;
      }},
      keyIterator : { value : function ()
      {
          return new ArrayIterator( this._keys ) ;
      }},
      keys : { value : function ()
      {
          return this._keys.concat() ;
      }},
      set : { value : function ( key , value )
      {
          let r = null ;
          let i = this.indexOfKey( key ) ;
          if ( i < 0 )
          {
              this._keys.push( key ) ;
              this._values.push( value ) ;
          }
          else
          {
              r = this._values[i] ;
              this._values[i] = value ;
          }
          return r ;
      }},
      setKeyAt : { value : function( index , key )
      {
          if ( index >= this._keys.length )
          {
              throw new RangeError( "ArrayMap.setKeyAt(" + index + ") failed with an index out of the range.") ;
          }
          if ( this.containsKey( key ) )
          {
              return null ;
          }
          let k = this._keys[index] ;
          if (k === undefined)
          {
              return null ;
          }
          let v = this._values[index] ;
          this._keys[index] = key ;
          return new MapEntry( k , v ) ;
      }},
      setValueAt : { value : function( index , value )
      {
          if ( index >= this._keys.length )
          {
              throw new RangeError( "ArrayMap.setValueAt(" + index + ") failed with an index out of the range.") ;
          }
          let v = this._values[index] ;
          if (v === undefined)
          {
              return null ;
          }
          let k = this._keys[index] ;
          this._values[index] = value ;
          return new MapEntry( k , v ) ;
      }},
      toString : { value : function () { return formatter.format( this ) ; }},
      values : { value : function ()
      {
          return this._values.concat() ;
      }}
  }) ;

  function InvalidChannelError( message , fileName , lineNumber )
  {
      this.name = 'InvalidChannelError';
      this.message    = message || 'invalid channel error' ;
      this.fileName   = fileName ;
      this.lineNumber = lineNumber ;
      this.stack      = (new Error()).stack;
  }
  InvalidChannelError.prototype = Object.create( Error.prototype );
  InvalidChannelError.prototype.constructor = InvalidChannelError;

  function fastformat( pattern , ...args )
  {
      if( (pattern === null) || !(pattern instanceof String || typeof(pattern) === 'string' ) )
      {
          return "" ;
      }
      if( args.length > 0 )
      {
          args = [].concat.apply( [] , args ) ;
          let len  = args.length ;
          for( let i = 0 ; i < len ; i++ )
          {
              pattern = pattern.replace( new RegExp( "\\{" + i + "\\}" , "g" ), args[i] );
          }
      }
      return pattern;
  }

  function InvalidFilterError( message , fileName , lineNumber )
  {
      this.name = 'InvalidFilterError';
      this.message    = message || 'invalid filter error' ;
      this.fileName   = fileName ;
      this.lineNumber = lineNumber ;
      this.stack      = (new Error()).stack;
  }
  InvalidFilterError.prototype = Object.create( Error.prototype );
  InvalidFilterError.prototype.constructor = InvalidFilterError;

  var strings = Object.defineProperties({}, {
    CHAR_PLACEMENT: {
      value: "'*' must be the right most character.",
      enumerable: true
    },
    CHARS_INVALID: {
      value: "The following characters are not valid\: []~$^&\/(){}<>+\=_-`!@#%?,\:;'\\",
      enumerable: true
    },
    EMPTY_FILTER: {
      value: "filter must not be null or empty.",
      enumerable: true
    },
    ERROR_FILTER: {
      value: "Error for filter '{0}'.",
      enumerable: true
    },
    DEFAULT_CHANNEL: {
      value: "",
      enumerable: true
    },
    ILLEGALCHARACTERS: {
      value: "[]~$^&/\\(){}<>+=`!#%?,:;'\"@",
      enumerable: true
    },
    INVALID_CHARS: {
      value: "Channels can not contain any of the following characters : []~$^&/\\(){}<>+=`!#%?,:;'\"@",
      enumerable: true
    },
    INVALID_LENGTH: {
      value: "Channels must be at least one character in length.",
      enumerable: true
    },
    INVALID_TARGET: {
      value: "Log, Invalid target specified.",
      enumerable: true
    }
  });

  function LoggerTarget() {
    Object.defineProperties(this, {
      _count: {
        value: 0,
        writable: true
      },
      _factory: {
        value: null,
        writable: true
      },
      _filters: {
        value: ["*"],
        writable: true
      },
      _level: {
        value: LoggerLevel.ALL,
        writable: true
      }
    });
    this.factory = Log;
  }
  LoggerTarget.prototype = Object.create(Receiver.prototype, {
    constructor: {
      value: LoggerTarget,
      writable: true
    },
    factory: {
      get: function get() {
        return this._factory;
      },
      set: function set(factory) {
        if (this._factory) {
          this._factory.removeTarget(this);
        }
        this._factory = factory instanceof LoggerFactory ? factory : Log;
        this._factory.addTarget(this);
      }
    },
    filters: {
      get: function get() {
        return [].concat(this._filters);
      },
      set: function set(value) {
        var filters = [];
        if (value && value instanceof Array && value.length > 0) {
          var filter;
          var length = value.length;
          for (var i = 0; i < length; i++) {
            filter = value[i];
            if (filters.indexOf(filter) === -1) {
              this._checkFilter(filter);
              filters.push(filter);
            }
          }
        } else {
          filters.push('*');
        }
        if (this._count > 0) {
          this._factory.removeTarget(this);
        }
        this._filters = filters;
        if (this._count > 0) {
          this._factory.addTarget(this);
        }
      }
    },
    level: {
      get: function get() {
        return this._level;
      },
      set: function set(value) {
        this._factory.removeTarget(this);
        this._level = value || LoggerLevel.ALL;
        this._factory.addTarget(this);
      }
    },
    addFilter: {
      value: function value(channel) {
        this._checkFilter(channel);
        var index = this._filters.indexOf(channel);
        if (index === -1) {
          this._filters.push(channel);
          return true;
        }
        return false;
      }
    },
    addLogger: {
      value: function value(logger) {
        if (logger && logger instanceof Logger) {
          this._count++;
          logger.connect(this);
        }
      }
    },
    logEntry: {
      value: function value(entry)
      {
      }
    },
    receive: {
      value: function value(entry) {
        if (entry instanceof LoggerEntry) {
          if (this._level === LoggerLevel.NONE) {
            return;
          }
          if (entry.level.valueOf() >= this._level.valueOf()) {
            this.logEntry(entry);
          }
        }
      }
    },
    removeFilter: {
      value: function value(channel) {
        if (channel && (typeof channel === "string" || channel instanceof String) && channel !== "") {
          var index = this._filters.indexOf(channel);
          if (index > -1) {
            this._filters.splice(index, 1);
            return true;
          }
        }
        return false;
      }
    },
    removeLogger: {
      value: function value(logger) {
        if (logger instanceof Logger) {
          this._count--;
          logger.disconnect(this);
        }
      }
    },
    _checkFilter: {
      value: function value(filter) {
        if (filter === null) {
          throw new InvalidFilterError(strings.EMPTY_FILTER);
        }
        if (this._factory.hasIllegalCharacters(filter)) {
          throw new InvalidFilterError(fastformat(strings.ERROR_FILTER, filter) + strings.CHARS_INVALID);
        }
        var index = filter.indexOf("*");
        if (index >= 0 && index !== filter.length - 1) {
          throw new InvalidFilterError(fastformat(strings.ERROR_FILTER, filter) + strings.CHAR_PLACEMENT);
        }
      }
    },
    toString: {
      value: function value() {
        return '[LoggerTarget]';
      }
    }
  });

  function LoggerFactory() {
    Object.defineProperties(this, {
      _loggers: {
        value: new ArrayMap(),
        writable: true
      },
      _targetLevel: {
        value: LoggerLevel.NONE,
        writable: true
      },
      _targets: {
        value: [],
        writable: true
      }
    });
  }
  LoggerFactory.prototype = Object.create(Receiver.prototype, {
    constructor: {
      value: LoggerFactory
    },
    addTarget: {
      value: function value(target
      )
      {
        if (target && target instanceof LoggerTarget) {
          var channel;
          var log;
          var filters = target.filters;
          var it = this._loggers.iterator();
          while (it.hasNext()) {
            log = it.next();
            channel = it.key();
            if (this._channelMatchInFilterList(channel, filters)) {
              target.addLogger(log);
            }
          }
          this._targets.push(target);
          if (this._targetLevel === LoggerLevel.NONE || target.level.valueOf() < this._targetLevel.valueOf()) {
            this._targetLevel = target.level;
          }
        } else {
          throw new Error(strings.INVALID_TARGET);
        }
      }
    },
    flush: {
      value: function value()
      {
        this._loggers.clear();
        this._targets = [];
        this._targetLevel = LoggerLevel.NONE;
      }
    },
    getLogger: {
      value: function value(channel) {
        this._checkChannel(channel);
        var logger = this._loggers.get(channel);
        if (!logger) {
          logger = new Logger(channel);
          this._loggers.set(channel, logger);
        }
        var target;
        var len = this._targets.length;
        for (var i = 0; i < len; i++) {
          target = this._targets[i];
          if (this._channelMatchInFilterList(channel, target.filters)) {
            target.addLogger(logger);
          }
        }
        return logger;
      }
    },
    hasIllegalCharacters: {
      value: function value(_value) {
        return indexOfAny(_value, strings.ILLEGALCHARACTERS.split("")) !== -1;
      }
    },
    isAll: {
      value: function value() {
        return this._targetLevel === LoggerLevel.ALL;
      }
    },
    isCritical: {
      value: function value() {
        return this._targetLevel === LoggerLevel.CRITICAL;
      }
    },
    isDebug: {
      value: function value() {
        return this._targetLevel === LoggerLevel.DEBUG;
      }
    },
    isError: {
      value: function value() {
        return this._targetLevel === LoggerLevel.ERROR;
      }
    },
    isInfo: {
      value: function value() {
        return this._targetLevel === LoggerLevel.INFO;
      }
    },
    isWarning: {
      value: function value() {
        return this._targetLevel === LoggerLevel.WARNING;
      }
    },
    isWtf: {
      value: function value() {
        return this._targetLevel === LoggerLevel.WTF;
      }
    },
    removeTarget: {
      value: function value(target) {
        if (target && target instanceof LoggerTarget) {
          var log;
          var filters = target.filters;
          var it = this._loggers.iterator();
          while (it.hasNext()) {
            log = it.next();
            var c = it.key();
            if (this._channelMatchInFilterList(c, filters)) {
              target.removeLogger(log);
            }
          }
          var len = this._targets.length;
          for (var i = 0; i < len; i++) {
            if (target === this._targets[i]) {
              this._targets.splice(i, 1);
              i--;
            }
          }
          this._resetTargetLevel();
        } else {
          throw new Error(strings.INVALID_TARGET);
        }
      }
    },
    toString: {
      value: function value() {
        return '[LoggerFactory]';
      }
    },
    _channelMatchInFilterList: {
      value: function value(channel, filters) {
        var filter;
        var index = -1;
        var len = filters.length;
        for (var i = 0; i < len; i++) {
          filter = filters[i];
          index = filter.indexOf("*");
          if (index === 0) {
            return true;
          }
          index = index < 0 ? index = channel.length : index - 1;
          if (channel.substring(0, index) === filter.substring(0, index)) {
            return true;
          }
        }
        return false;
      }
    },
    _checkChannel: {
      value: function value(channel)
      {
        if (channel === null || channel.length === 0) {
          throw new InvalidChannelError(strings.INVALID_LENGTH);
        }
        if (this.hasIllegalCharacters(channel) || channel.indexOf("*") !== -1) {
          throw new InvalidChannelError(strings.INVALID_CHARS);
        }
      }
    },
    _resetTargetLevel: {
      value: function value() {
        var t;
        var min = LoggerLevel.NONE;
        var len = this._targets.length;
        for (var i = 0; i < len; i++) {
          t = this._targets[i];
          if (min === LoggerLevel.NONE || t.level.valueOf() < min.valueOf()) {
            min = t.level;
          }
        }
        this._targetLevel = min;
      }
    }
  });

  var Log = new LoggerFactory();

  const leading = ( value , count = 2 , char = '0' ) =>
  {
      const isNegative = Number(value) < 0;
      count = count > 0 ? count : 2 ;
      let buffer = value.toString();
      if( isNegative )
      {
          buffer = buffer.slice(1);
      }
      const size = count - buffer.length + 1;
      buffer = new Array(size).join(char).concat(buffer);
      return ( isNegative ? '-' : '' ) + buffer ;
  };

  function LineFormattedTarget() {
    var init = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    LoggerTarget.call(this);
    Object.defineProperties(this, {
      _lineNumber: {
        value: 1,
        writable: true
      }
    });
    this.includeChannel = false;
    this.includeDate = false;
    this.includeLevel = false;
    this.includeLines = false;
    this.includeMilliseconds = false;
    this.includeTime = false;
    this.separator = " ";
    if (init) {
      for (var prop in init) {
        if (this.hasOwnProperty(prop)) {
          this[prop] = init[prop];
        }
      }
    }
  }
  LineFormattedTarget.prototype = Object.create(LoggerTarget.prototype, {
    constructor: {
      value: LineFormattedTarget,
      writable: true
    },
    internalLog: {
      value: function value(message, level)
      {
      }
    },
    toString: {
      writable: true,
      value: function value() {
        return '[' + this.constructor.name + ']';
      }
    },
    logEntry: {
      value: function value(entry) {
        var message = this.formatMessage(entry.message, LoggerLevel.getLevelString(entry.level), entry.channel, new Date());
        this.internalLog(message, entry.level);
      }
    },
    resetLineNumber: {
      value: function value() {
        this._lineNumber = 1;
      }
    },
    formatDate: {
      value: function value(d) {
        var date = "";
        date += leading(d.getDate());
        date += "/" + leading(d.getMonth() + 1);
        date += "/" + d.getFullYear();
        return date;
      }
    },
    formatLevel: {
      value: function value(level) {
        return '[' + level + ']';
      }
    },
    formatLines: {
      value: function value() {
        return "[" + this._lineNumber++ + "]";
      }
    },
    formatMessage: {
      value: function value(message, level, channel, date
      ) {
        var msg = "";
        if (this.includeLines) {
          msg += this.formatLines() + this.separator;
        }
        if (this.includeDate || this.includeTime) {
          date = date || new Date();
          if (this.includeDate) {
            msg += this.formatDate(date) + this.separator;
          }
          if (this.includeTime) {
            msg += this.formatTime(date) + this.separator;
          }
        }
        if (this.includeLevel) {
          msg += this.formatLevel(level || "") + this.separator;
        }
        if (this.includeChannel) {
          msg += (channel || "") + this.separator;
        }
        msg += message;
        return msg;
      }
    },
    formatTime: {
      value: function value(d) {
        var time = "";
        time += leading(d.getHours());
        time += ":" + leading(d.getMinutes());
        time += ":" + leading(d.getSeconds());
        if (this.includeMilliseconds) {
          time += ":" + leading(d.getMilliseconds());
        }
        return time;
      }
    }
  });

  function ConsoleTarget() {
    var init = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    LineFormattedTarget.call(this, init);
  }
  ConsoleTarget.prototype = Object.create(LineFormattedTarget.prototype, {
    constructor: {
      value: ConsoleTarget
    },
    internalLog: {
      value: function value(message, level)
      {
        if (console) {
          switch (level) {
            case LoggerLevel.CRITICAL:
              {
                console.trace(message);
                break;
              }
            case LoggerLevel.DEBUG:
              {
                console.log(message);
                break;
              }
            case LoggerLevel.ERROR:
              {
                console.error(message);
                break;
              }
            case LoggerLevel.INFO:
              {
                console.info(message);
                break;
              }
            case LoggerLevel.WARNING:
              {
                console.warn(message);
                break;
              }
            default:
            case LoggerLevel.ALL:
              {
                console.log(message);
                break;
              }
          }
        } else {
          throw new new ReferenceError('The console reference is unsupported.')();
        }
      }
    }
  });

  function TraceTarget() {
    var init = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    LineFormattedTarget.call(this, init);
  }
  TraceTarget.prototype = Object.create(LineFormattedTarget.prototype, {
    constructor: {
      value: TraceTarget
    },
    internalLog: {
      value: function value(message, level)
      {
        console.log(message);
      }
    }
  });

  /**
   * The {@link system.logging} library defines functions and classes which implement a flexible event logging system for applications and libraries.
   * @summary The {@link system.logging} library defines functions and classes which implement a flexible event logging system for applications and libraries.
   * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
   * @author Marc Alcaraz <ekameleon@gmail.com>
   * @namespace system.logging
   * @memberof system
   * @example
   * var logger = Log.getLogger('channel') ;
   *
   * var target = new ConsoleTarget
   * ({
   *     includeChannel      : true  ,
   *     includeDate         : false ,
   *     includeLevel        : true  ,
   *     includeLines        : true  ,
   *     includeMilliseconds : true  ,
   *     includeTime         : true
   * }) ;
   *
   * target.filters = ['*'] ;
   * target.level   = LoggerLevel.ALL ;
   *
   * logger.debug( 'hello {0}, love it.' , 'VEGAS' ) ;
   * logger.critical( 'hello {0}, it\'s critical.' , 'VEGAS' ) ;
   * logger.info( 'hello, my name is {0}' , 'VEGAS' ) ;
   * logger.error( 'hello {0}, an error is invoked.' , 'VEGAS' ) ;
   * logger.warning( 'hello {0}, don\'t forget me.' , 'VEGAS' ) ;
   * logger.wtf( 'hello {0} ! WHAT ??' , 'VEGAS' ) ;
   */
  var data = {
    isLoggable: isLoggable,
    Log: Log,
    Loggable: Loggable,
    Logger: Logger,
    LoggerEntry: LoggerEntry,
    LoggerFactory: LoggerFactory,
    LoggerLevel: LoggerLevel,
    LoggerTarget: LoggerTarget,
    targets: {
      ConsoleTarget: ConsoleTarget,
      LineFormattedTarget: LineFormattedTarget,
      TraceTarget: TraceTarget
    }
  };

  var skip = false ;
  function sayHello( name = '' , version = '' , link = '' )
  {
      if( skip )
      {
          return ;
      }
      try
      {
          if ( navigator && navigator.userAgent && navigator.userAgent.toLowerCase().indexOf('chrome') > -1)
          {
              const args = [
                  `\n %c %c %c ${name} ${version} %c %c ${link} %c %c\n\n`,
                  'background: #ff0000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'color: #F7FF3C; background: #000000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'color: #F7FF3C; background: #ff0000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'background: #ff0000; padding:5px 0;',
              ];
              window.console.log.apply( console , args );
          }
          else if (window.console)
          {
              window.console.log(`${name} ${version} - ${link}`);
          }
      }
      catch( error )
      {
      }
  }
  function skipHello()
  {
      skip = true ;
  }

  var metas = Object.defineProperties({}, {
    name: {
      enumerable: true,
      value: 'vegas-js-logging'
    },
    description: {
      enumerable: true,
      value: 'A library allow you to implement a flexible event logging system for applications and libraries.'
    },
    version: {
      enumerable: true,
      value: '1.0.2'
    },
    license: {
      enumerable: true,
      value: "MPL-2.0 OR GPL-2.0+ OR LGPL-2.1+"
    },
    url: {
      enumerable: true,
      value: 'https://bitbucket.org/ekameleon/vegas-js-logging'
    }
  });
  var bundle = _objectSpread({
    metas: metas,
    sayHello: sayHello,
    skipHello: skipHello
  }, data);
  try {
    if (window) {
      window.addEventListener('load', function load() {
        window.removeEventListener("load", load, false);
        sayHello(metas.name, metas.version, metas.url);
      }, false);
    }
  } catch (ignored) {}

  return bundle;

})));
//# sourceMappingURL=vegas.logging.js.map
