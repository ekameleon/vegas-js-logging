/* globals vegas_logging */

window.addEventListener( 'load' , function load()
{
    window.removeEventListener( "load", load, false ) ;
    
    if( !vegas_logging )
    {
        throw new Error("The VEGAS library is not found.") ;
    }
    
    const logging = vegas_logging ;
    
    const { Log , LoggerLevel , targets }  = logging ;
    
    const { ConsoleTarget } = targets ;
    
    
    
    var logger = Log.getLogger('application') ;

    console.log( ConsoleTarget ) ;

    let target = new ConsoleTarget
    ({
        includeChannel      : false  ,
        includeDate         : false ,
        includeLevel        : true  ,
        includeLines        : true  ,
        includeMilliseconds : true  ,
        includeTime         : true
    }) ;

    target.filters = ['*'] ;
    target.level   = LoggerLevel.ALL ;

    logger.debug( 'hello {0}, love it.'  , 'VEGAS' ) ;
    logger.info( 'hello, my name is {0}' , 'VEGAS' ) ;

    logger.log( 'hello {0}, simple log.' , 'VEGAS' ) ;
    logger.wtf( 'hello {0} ! WHAT ??'    , 'VEGAS' ) ;

    logger.critical( 'hello {0}, it\'s critical.'   , 'VEGAS' ) ;
    logger.error( 'hello {0}, an error is invoked.' , 'VEGAS' ) ;
    logger.warning( 'hello {0}, don\'t forget me.'  , 'VEGAS' ) ;

});