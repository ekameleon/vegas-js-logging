"use strict" ;

import strings from '../../src/strings'

import chai  from 'chai' ;
const assert = chai.assert ;

describe( 'system.logging.strings' , () =>
{
    it('strings.CHAR_PLACEMENT', () =>
    {
        assert.equal( strings.CHAR_PLACEMENT , "'*' must be the right most character." );
    });
    
    it('strings.CHARS_INVALID', () =>
    {
        assert.equal( strings.CHARS_INVALID , "The following characters are not valid\: []~$^&\/(){}<>+\=_-`!@#%?,\:;'\\" );
    });
   
    it('strings.EMPTY_FILTER', () =>
    {
        assert.equal( strings.EMPTY_FILTER , "filter must not be null or empty." );
    });
    
    it('strings.EMPTY_FILTER', () =>
    {
        assert.equal( strings.ERROR_FILTER , "Error for filter '{0}'." );
    });
    
    it('strings.DEFAULT_CHANNEL', () =>
    {
        assert.equal( strings.DEFAULT_CHANNEL , "" );
    });
    
    it('strings.ILLEGALCHARACTERS', () =>
    {
        assert.equal( strings.ILLEGALCHARACTERS , "[]~$^&/\\(){}<>+=`!#%?,:;'\"@" );
    });
    
    it('strings.INVALID_CHARS', () =>
    {
        assert.equal( strings.INVALID_CHARS , "Channels can not contain any of the following characters : []~$^&/\\(){}<>+=`!#%?,:;'\"@" );
    });
    
    it('strings.INVALID_LENGTH', () =>
    {
        assert.equal( strings.INVALID_LENGTH , "Channels must be at least one character in length." );
    });
    
    it('strings.INVALID_TARGET', () =>
    {
        assert.equal( strings.INVALID_TARGET , "Log, Invalid target specified." );
    });
});